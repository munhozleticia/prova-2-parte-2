package Controllers;

//Imports
import java.util.ArrayList;
import javax.swing.JOptionPane;

import Model.*;


public class Controller {

	//Definindo Array
	static ArrayList <Medico> listaMed;
	static ArrayList <Paciente> listaPac;
	
	
	public Controller(){
		listaMed = new ArrayList<Medico>();
		listaPac = new ArrayList<Paciente>();
	}
	

	public void adicionarF(Medico umMedico){
		listaMed.add(umMedico);
	}
	
	public void adicionarT(Paciente umPaciente){
		listaPac.add(umPaciente);
	}	
	
	public void removerT(Paciente umPaciente){
		if (umPaciente == null)
		{ 
			JOptionPane.showMessageDialog(null, "Nao encontrado.");
			
		}else
		{
		listaPac.remove(umPaciente);
		}
	}
	
	public void removerF(Medico umMedico){
		if (umMedico == null)
		{ 
		
		}else
		{
		listaMed.remove(umMedico);
		} 
	}
	
	
		public boolean remover(String nome){
			Paciente umPaciente = null;
			Medico umMedico = null;
		
	umMedico = pesquisarF(nome);
	umPaciente = pesquisarT(nome);
	
	if ((umMedico == null) && (umPaciente == null)) {
		return false;
	} else{
	if ((umMedico != null) && (umPaciente != null)) 
		{
		
		removerF(umMedico);
		removerT(umPaciente);
		return true;
	} else{
	if (umPaciente != null)
	{
		removerT(umPaciente);
		return true;
	} else {
		removerF(umMedico);
		return true;
	}
		}
			}
		}
	

	
	public Medico pesquisarF(String umNome){
		for(Medico umMedico : listaMed){
			if(umMedico.getNome().equalsIgnoreCase(umNome))
			return umMedico;
		}
		return null;
	}
	
	public Paciente pesquisarT(String umNome){
		for(Paciente umPaciente : listaPac){
			if(umPaciente.getNome().equalsIgnoreCase(umNome))
				return umPaciente;
		}
		return null;
	}
	
	
	public String exibirF(){
		String saida= "Medico";
		if(listaMed.size()>0){
		for(Medico umMedico : listaMed){
			System.out.println(umMedico.getNome());
			saida = (saida + "\n" + umMedico.getNome());
		} 
		}
		return saida; 
	}
	
	public String exibirT(){
		String saida= "Paciente"; 
		if(listaPac.size()>0){
		for(Paciente umPaciente : listaPac){
			System.out.println(umPaciente.getNome());
			saida = (saida + "\n" + umPaciente.getNome());
		} 
	
		} 
		return saida;
	}
	
	public String caractF(Medico umMedico){
	ConsultaMedica umaConsulta =  umMedico.getUmaConsultaMedica();
		String med = ("Medico" + "\n" + "Nome : " + umMedico.getNome() + "\n" +
				"Especialidade : " + umMedico.getEspecialidade() + "\n" + "RG : " + umMedico.getRg() + "\n" +
				"Data : " + umaConsulta.getData() + "\n" + "Horario : " + umaConsulta.getHorario() + "\n" + 
				"Diagnostico : " + umaConsulta.getDiagnostico() + "\n" + "CPF : " + umMedico.getCpf() + "\n" + 
				"Registro Medico : " + umMedico.getRegistroMedico() );
		return med;
	}
	
	public String caractT(Paciente umPaciente){
	ConsultaMedica umaConsulta =  umPaciente.getUmaConsultaMedica();
	String pac = ("Paciente" + "\n" + "Nome : " + umPaciente.getNome() + "\n" +
			"Sintoma : " + umPaciente.getIdade() + "\n" + "RG : " + umPaciente.getRg() + "\n" +
			"Data : " + umaConsulta.getData() + "\n" + "Horario : " + umaConsulta.getHorario() + "\n" + 
			"Diagnostico : " + umaConsulta.getDiagnostico() + "\n" + "CPF : " + umPaciente.getCpf() + "\n" +
			"Plano de Saude : " + umPaciente.getPlanoSaude() + "\n" +  "Idade : " + umPaciente.getIdade() );
	return pac;
	}
	
	public String caract(String umNome){
		Paciente umPaciente = null;
		Medico umMedico = null;
		String saida = "Default";
	
umMedico = pesquisarF(umNome);
umPaciente = pesquisarT(umNome);

if ((umMedico == null) && (umPaciente == null)) {
 	saida = "Nao encontrado";
		} else{

if (umPaciente != null)
{
	saida = caractT(umPaciente);
}
if (umMedico != null)
{
	saida = caractF(umMedico);
}
if ((umMedico != null) && (umPaciente != null))
{
	saida = (caractF(umMedico) + "\n\n" + caractT(umPaciente));
}
		}
return saida;
	}
	
	
}