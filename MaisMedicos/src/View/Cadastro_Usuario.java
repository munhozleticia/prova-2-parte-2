package View;


import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyledDocument;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.awt.Font;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import Model.ConsultaMedica;
import Model.Medico;
import Model.Paciente;

import java.awt.event.ActionListener;

import Model.*;
import Controllers.*;

import javax.swing.JTextPane;



public class Cadastro_Usuario extends JFrame {

	private JPanel contentPane;
	
	private JTextField textNome;
	private JTextField textCpf;
	private JTextField textRg;
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	
	private JTextField textRegistroMedico;
	private JTextField textEspecialidade;
	
	private JTextField textIdade;
	private JTextField textSintoma;
	private JTextField textPlanoSaude;
	
	private JTextField textData;
	private JTextField textHorario;
	private JTextField textDiagnostico;
	//private final Action action = new umaAction();
	
	private	boolean med = false;
	private boolean pac = false;
	Medico umMedico = null;
	Paciente umPaciente = null;
	ConsultaMedica umaConsulta = null;
	private JTextField textDados;

	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cadastro_Usuario frame = new Cadastro_Usuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Cadastro_Usuario() {
		
		final Controller umController = new Controller();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 429);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDesejaCadastrarQual = new JLabel("Deseja cadastrar qual usuario?");
		lblDesejaCadastrarQual.setBounds(12, 12, 220, 15);
		contentPane.add(lblDesejaCadastrarQual);
		
		
		final JLabel lblNome = new JLabel("Nome");
		lblNome.setEnabled(false);
		lblNome.setBounds(372, 31, 70, 15);
		contentPane.add(lblNome);
		
		textNome = new JTextField();
		textNome.setEnabled(false);
		textNome.setBounds(289, 43, 199, 19);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		final JLabel lblCpf = new JLabel("CPF");
		lblCpf.setEnabled(false);
		lblCpf.setBounds(372, 60, 70, 15);
		contentPane.add(lblCpf);
		
		textCpf = new JTextField();
		textCpf.setEnabled(false);
		textCpf.setBounds(338, 74, 114, 19);
		contentPane.add(textCpf);
		textCpf.setColumns(10);
		
		final JLabel lblRg = new JLabel("RG");
		lblRg.setEnabled(false);
		lblRg.setBounds(372, 94, 70, 15);
		contentPane.add(lblRg);
		
		textRg = new JTextField();
		textRg.setEnabled(false);
		textRg.setBounds(338, 109, 114, 19);
		contentPane.add(textRg);
		textRg.setColumns(10);
		
		final JLabel lblData = new JLabel("DATA");
		lblData.setEnabled(false);
		lblData.setBounds(382, 228, 77, 15);
		contentPane.add(lblData);
		
		textData = new JTextField();
		textData.setEnabled(false);
		textData.setBounds(338, 241, 114, 19);
		contentPane.add(textData);
		textData.setColumns(10);
		
		final JLabel lblHorario = new JLabel("Horario");
		lblHorario.setEnabled(false);
		lblHorario.setBounds(368, 259, 70, 15);
		contentPane.add(lblHorario);
		
		textHorario = new JTextField();
		textHorario.setEnabled(false);
		textHorario.setBounds(338, 272, 114, 19);
		contentPane.add(textHorario);
		textHorario.setColumns(10);
		
		final JLabel lblDiagnostico = new JLabel("Diagnostico");
		lblDiagnostico.setEnabled(false);
		lblDiagnostico.setBounds(372, 289, 70, 15);
		contentPane.add(lblDiagnostico);
		
		textDiagnostico = new JTextField();
		textDiagnostico.setEnabled(false);
		textDiagnostico.setColumns(10);
		textDiagnostico.setBounds(338, 303, 114, 19);
		contentPane.add(textDiagnostico);
		
		final JLabel lblConsulta = new JLabel("CONSULTA");
		lblConsulta.setEnabled(false);
		lblConsulta.setFont(new Font("Dialog", Font.BOLD, 13));
		lblConsulta.setBounds(356, 201, 96, 15);
		contentPane.add(lblConsulta);
		
		final JTextField textDados = new JTextField();
		textDados.setEnabled(false);
		textDados.setBounds(50, 210, 182, 19);
		contentPane.add(textDados);
		textDados.setColumns(10);
		
		final JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setEnabled(false);
		textPane.setBounds(12, 272, 265, 141);
		contentPane.add(textPane);
		
		
		// BOTOES
		
		final JButton btnMedico = new JButton("Medico");
		final JButton btnPaciente = new JButton("Paciente");
		final JButton btnSubmeter = new JButton("Submeter");
		final JButton btnListarUsuario = new JButton("Listar Usuarios");
		
		
		
		JButton btnCadastrarPaciente = new JButton("Cadastrar Paciente");
		btnCadastrarPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
			}
/*
			JButton btnCadastrarPaciente = new JButton("Cadastrar Paciente");
			btnCadastrarMedico.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
	*/			
			private void limparCampos() {
				textNome.setText("");
				textNome.setText("");
				textRg.setText("");
				textCpf.setText("");
				textIdade.setText("");
				textSintoma.setText("");
				textPlanoSaude.setText("");
				textData.setText("");
				textHorario.setText("");
				textDiagnostico.setText("");
				
							
			}
		});
		btnCadastrarPaciente.setBounds(12, 357, 200, 30);
		contentPane.add(btnCadastrarPaciente);

		
		
		
		btnListarUsuario.setEnabled(false);
		btnListarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),umController.exibirT() + "\n" + umController.exibirF(), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			}

			
		});
		btnListarUsuario.setBounds(140, 141, 130, 25);
		contentPane.add(btnListarUsuario);
		
		final JButton btnRemoverUsuario = new JButton("Remover");
		btnRemoverUsuario.setEnabled(false);
		btnRemoverUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(umController.remover(textDados.getText()))
				JOptionPane.showMessageDialog(null, "Removido com Sucesso!");
				else
					JOptionPane.showMessageDialog(null, "Nao Encontrado");
			}
		});
		btnRemoverUsuario.setBounds(12, 238, 117, 25);
		contentPane.add(btnRemoverUsuario);
		
		
		final JButton btnBuscarUsuario = new JButton("Buscar Usuario");
		btnBuscarUsuario.setEnabled(false);
		btnBuscarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				textPane.setText("");
			StyledDocument doc = textPane.getStyledDocument();
			try {
				doc.insertString(doc.getLength(),umController.caract(textDados.getText()), null);

			} 
			catch(Exception e) { JOptionPane.showMessageDialog(null, "Erro");}
			}
		});
		btnBuscarUsuario.setFont(new Font("Dialog", Font.BOLD, 11));
		btnBuscarUsuario.setBounds(143, 239, 120, 25);
		contentPane.add(btnBuscarUsuario);
		
		final JLabel lblRemoveBusca = new JLabel("Usuario a remover ou Buscar:");
		lblRemoveBusca.setEnabled(false);
		lblRemoveBusca.setBounds(33, 178, 254, 34);
		contentPane.add(lblRemoveBusca);
		
	
		
		
		btnSubmeter.addActionListener (new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				if (med){
					umMedico = new Medico(textNome.getText(), textEspecialidade.getText());
					umMedico.setCpf(textCpf.getText());
					umMedico.setRg(textRg.getText());
					umMedico.setRegistroMedico(textRegistroMedico.getText());

					umaConsulta = new ConsultaMedica(textData.getText());
					umaConsulta.setHorario(textHorario.getText());
					umaConsulta.setDiagnostico(textDiagnostico.getText());
					
					umMedico.setUmaConsultaMedica(umaConsulta);
					umController.adicionarF(umMedico);	
					
					textNome.setText("");
					textCpf.setText("");
					textRg.setText("");
					
					textRegistroMedico.setText("");
					textEspecialidade.setText("");
					textData.setText("");
					textHorario.setText("");
					textDiagnostico.setText("");
					
					
					JOptionPane.showMessageDialog(null, "Medico adicionado com sucesso!");
				}
				if (pac){
					umPaciente = new Paciente(textNome.getText(), textSintoma.getText());
					umPaciente.setIdade(textIdade.getText());
					umPaciente.setPlanoSaude(textPlanoSaude.getText());
					umaConsulta = new ConsultaMedica(textData.getText());
					umaConsulta.setHorario(textHorario.getText());
					umaConsulta.setDiagnostico(textDiagnostico.getText());
					
					umPaciente.setUmaConsultaMedica(umaConsulta);
					umController.adicionarT(umPaciente);	
					
					textNome.setText("");
					textRg.setText("");
					textCpf.setText("");

					textIdade.setText("");
					textSintoma.setText("");
					textPlanoSaude.setText("");
					textData.setText("");
					textHorario.setText("");
					textDiagnostico.setText("");
					
					
					JOptionPane.showMessageDialog(null, "Paciente adicionado com sucesso!");
				}
			}
		});
		btnSubmeter.setEnabled(false);
		btnSubmeter.setBounds(12, 141, 117, 25);
		contentPane.add(btnSubmeter);
		
		
		
		
		btnMedico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNome.setEnabled(true);
				textNome.setEnabled(true);
				lblCpf.setEnabled(true);
				textCpf.setEnabled(true);
				lblRg.setEnabled(true);
				textRg.setEnabled(true);
				
				//lblEspecialidade.setEnabled(true);
				textEspecialidade.setEnabled(true);
				lblData.setEnabled(true);
				lblHorario.setEnabled(true);
				textData.setEnabled(true);
				lblHorario.setEnabled(true);
				textHorario.setEnabled(true);
				lblDiagnostico.setEnabled(true);
				textDiagnostico.setEnabled(true);
				textEspecialidade.setText("Especialidade");
				btnMedico.setEnabled(false);
				btnPaciente.setEnabled(true);
				btnSubmeter.setEnabled(true);
				
				btnListarUsuario.setEnabled(true);
				btnRemoverUsuario.setEnabled(true);
				textDados.setEnabled(true);
				textPane.setEnabled(true);
				btnBuscarUsuario.setEnabled(true);
				lblRemoveBusca.setEnabled(true);
				
				med = true;
				pac = false;
				
			}
		});
		btnMedico.setBounds(65, 39, 117, 25);
		contentPane.add(btnMedico);
		

		btnPaciente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblNome.setEnabled(true);
				textNome.setEnabled(true);
				textIdade.setEnabled(true);
				textPlanoSaude.setEnabled(true);
				//lblSintoma.setEnabled(true);
				textSintoma.setEnabled(true);
				
				//lblEspecialidade.setEnabled(true);
				textEspecialidade.setEnabled(true);
				//lblEndereco.setEnabled(true);
				//lblCep.setEnabled(true);
				textData.setEnabled(true);
				//lblEstado.setEnabled(true);
				textHorario.setEnabled(true);
				//lblCidade.setEnabled(true);
				textDiagnostico.setEnabled(true);
				//lblEspecifico.setText("Forca saque 1-5");
				btnMedico.setEnabled(true);
				btnPaciente.setEnabled(false);
				btnSubmeter.setEnabled(true);
				
				btnListarUsuario.setEnabled(true);
				btnRemoverUsuario.setEnabled(true);
				textDados.setEnabled(true);
				textPane.setEnabled(true);
				btnBuscarUsuario.setEnabled(true);
				lblRemoveBusca.setEnabled(true);
				
				pac = true;
				med = false;
			}
		});
		btnPaciente.setBounds(65, 75, 117, 25);
		contentPane.add(btnPaciente);
		
		JLabel lblCadastro = new JLabel("CADASTRO");
		lblCadastro.setBounds(356, 7, 82, 15);
		contentPane.add(lblCadastro);
	
	/*private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}*/
}
		}
	