package View;

import java.util.Scanner;

import Controllers.Controller;
import Model.ConsultaMedica;
import Model.Medico;
import Model.Paciente;

//Classe que faz a "ligacao" das outras com o usuario... pertence a VIEW
public class CadastroUsuario {


	public static void main(String[] args) {
		// Opcao inicial para o loop
		int opcao = -1;
		int opcao2 = -1;
		
		// Classe para ler da tela
		Scanner read = new Scanner(System.in);
		
		Medico umMedico = null;
		Paciente umPaciente = null;
		
		ConsultaMedica umaConsulta = null;
		
		Controller umControlador = new Controller();
		Menu menu = new Menu();

		while(opcao != 0){
			menu.menuInicial();
			opcao = read.nextInt();
			read.nextLine(); //Limpar buffer
			
			switch(opcao){
			
			case 1:
				
				// Adicionar Usuario
				while(opcao2 != 0) {
					
					System.out.println(" \nDigite:"
							+ "\n 1 - para cadastrar um Medico "
							+ "\n 2 - para cadastrar um Paciente"
							+ "\n 0 - para sair da tela de cadastro");
					opcao2 = read.nextInt();
					read.nextLine(); //Limpar buffer
					
					switch(opcao2){
					case 0:
						break;
						
					case 1:
						
				System.out.println("Informe o nome do usuario a ser cadastrado:");
				String nome = read.nextLine();
				
				System.out.println("Informe o cpf do usuario a ser cadastrado:");
				String cpf = read.nextLine();
				
				System.out.println("Informe o rg do usuario a ser cadastrado:");
				String rg = read.nextLine();
				
				System.out.println("Informe o numero do registro do medico");
				String registroMedico = read.nextLine();
				umMedico = new Medico(nome, registroMedico);
				
				System.out.println("Informe a especialidade do medico:");
				umMedico.setEspecialidade(read.next());
				
				// Instanciar Consulta
				
				System.out.println("---CONSULTA---");
				
				System.out.println("Informe a data:");
				String data = read.next();
				umaConsulta = new ConsultaMedica(data);

				
				System.out.println("Informe o horario:");
				umaConsulta.setHorario(read.next());
				
				System.out.println("Informe o diagnostico:");
				umaConsulta.setDiagnostico(read.next());
				
				// Passando Consulta para Usuario.
				umMedico.setUmaConsultaMedica(umaConsulta);
				umControlador.adicionarF(umMedico);
				opcao2=0;
				break;
				
					case 2:
					
						System.out.println("Informe o nome do usuario a ser cadastrado:");
						String Nome = read.nextLine();
						
						System.out.println("Informe o cpf do usuario a ser cadastrado:");
						String Cpf = read.nextLine();
						
						System.out.println("Informe o rg do usuario a ser cadastrado:");
						String Rg = read.nextLine();
						
						System.out.println("Informe o sintoma do paciente:");
						String Sintoma = read.nextLine();
						umPaciente = new Paciente(Nome, Sintoma);
						
						System.out.println("Informe a Idade desse paciente:");
						String Idade = read.nextLine();
						
						System.out.println("Informe o plano de saude desse paciente:");
						umPaciente.setPlanoSaude(read.next());
									
						// Instanciar Consulta
						
						System.out.println("---CONSULTA---");
						
						System.out.println("Informe a data:");
						String Data = read.next();
						umaConsulta = new ConsultaMedica(Data);

						
						System.out.println("Informe o horario:");
						umaConsulta.setHorario(read.next());
						
						System.out.println("Informe o diagnostico:");
						umaConsulta.setDiagnostico(read.next());
						
						// Passando Consulta para Usuario.
						umPaciente.setUmaConsultaMedica(umaConsulta);
						umControlador.adicionarT(umPaciente);
						opcao2=0;
						break;

					default :
					System.out.println("\nDigite novamente. ");
					}
				}
			if(opcao2==0)
			{
				opcao2=-1;
				break;
			}
				
			case 2:
				//Remover Usuario
				umControlador.exibirF();
				umControlador.exibirT();
				System.out.println("\nDigite o Nome do Usuario a ser deletado: ");
				String nome = read.nextLine();
				umMedico = umControlador.pesquisarF(nome);
				umPaciente = umControlador.pesquisarT(nome);
				
				if ((umMedico == null) && (umPaciente == null)) {
						System.out.println("Nao encontrado");
						} else{
				if ((umMedico != null) && (umPaciente != null)) 
					{
					umControlador.removerF(umMedico);
					umControlador.removerT(umPaciente);
					System.out.println("Medico e Paciente Deletado");
					} else{
				if (umPaciente == null)
				{
					umControlador.removerF(umMedico);
					System.out.println("Medico Deletado");
				} else {
					umControlador.removerT(umPaciente);
					System.out.println("Paciente Deletado");
				}
					}
						}
				
			break;
			
			case 3:
				// Exibir Usuario
				if((umMedico==null) && (umPaciente == null)){
					System.out.println("Nenuhum usuario encontrado.");
				}else{
				umControlador.exibirT();
				umControlador.exibirF();
				System.out.println("   Para ver os dados de cada um acesse Pesquisa de Usuario.");
				}

			break;
			
			case 4:
				// Listar Usuario
				System.out.println("\t Pesquisa de Usuario");

				System.out.println("\nDigite o nome do usuario a ser pesquisado: ");
				nome= read.nextLine();
				Medico oMed = umControlador.pesquisarF(nome);
				Paciente oPac = umControlador.pesquisarT(nome);
				if ((umMedico == null) && (umPaciente == null)) {
					System.out.println("Nao encontrado");
					} else{
			if ((umMedico != null) && (umPaciente != null)) 
				{
				System.out.println("Medico e paciente com o mesmo nome");
				umControlador.caractF(oMed);
	            umControlador.caractT(oPac);
				} else{
			if (umPaciente != null)
			{
	            umControlador.caractT(oPac);
			} else {
				umControlador.caractF(oMed);
			}
				}
					}
				break;			
			
			case 0:
				System.out.println("Fechando programa...");
				System.out.println("Programa Fechado Com Sucesso!");
				System.exit(0);
				
			break;
			
			default:
				System.out.println("Opcao invalida! Digite outra: ");
			break;
			
			}
		}
	}

}