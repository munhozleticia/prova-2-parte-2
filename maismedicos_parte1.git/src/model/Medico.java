package model;

public class Medico extends Usuario{

	private String crm;
	private int anosExperiencia;
	private String hospitalTrabalho;
	
	public String getCrm() {
		return crm;
	}
	
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public int getAnosExperiencia() {
		return anosExperiencia;
	}
	
	public void setAnosExperiencia(int anosExperiencia) {
		this.anosExperiencia = anosExperiencia;
	}
	
	public String getHospitalTrabalho() {
		return hospitalTrabalho;
	}
	
	public void setHospitalTrabalho(String hospitalTrabalho) {
		this.hospitalTrabalho = hospitalTrabalho;
	}
	
	
}
