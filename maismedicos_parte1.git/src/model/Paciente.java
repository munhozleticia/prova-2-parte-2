package model;

public class Paciente extends Usuario{

	
	private String doenca;
	private String inicioTratamento;
	private String medicoResponsavel;
	private String medicamentoUtilizado;
	
	public String getDoenca() {
		return doenca;
	}
	
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	
	public String getInicioTratamento() {
		return inicioTratamento;
	}
	
	public void setInicioTratamento(String inicioTratamento) {
		this.inicioTratamento = inicioTratamento;
	}
	
	public String getMedicoResponsavel() {
		return medicoResponsavel;
	}
	
	public void setMedicoResponsavel(String medicoResponsavel) {
		this.medicoResponsavel = medicoResponsavel;
	}
	
	public String getMedicamentoUtilizado() {
		return medicamentoUtilizado;
	}
	
	public void setMedicamentoUtilizado(String medicamentoUtilizado) {
		this.medicamentoUtilizado = medicamentoUtilizado;
	}
	
	
}
