import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextPane;
import javax.swing.JTabbedPane;


public class Interface extends JFrame {

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Interface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 772, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.NORTH);
		
		tabbedPane.addTab(titulo, componente);
		tabbedPane.addTab(titulo, icone, componente);
		tabbedPane.addTab(titulo, icone, componente, indice);
		
		public TabbedPaneDemo01() {
			this.setTitle("Testando JTabbedPane");
			// Cria o JTabbedPane
			JTabbedPane tabbedPane = new JTabbedPane();
			// Prepara a inser��o dos paineis
			JPanel painelTerra = new JPanel();
			painelTerra.add(new JLabel(
			new ImageIcon("Earth.gif")));
			JPanel painelMarte = new JPanel();
			painelMarte.add(new JLabel(
			new ImageIcon("Mars.gif")));
			JPanel painelJupiter = new JPanel();
			painelJupiter.add(new JLabel(
			new ImageIcon("Jupiter.gif")));
			
			tabbedPane.addTab("Terra", 
					new ImageIcon("left.gif"), 
					painelTerra, "Planeta Terra");
					tabbedPane.addTab("Marte", 
					new ImageIcon("middle.gif"), 
					painelMarte, "Planeta Marte");
					tabbedPane.addTab("Jupiter", 
					new ImageIcon("right.gif"), 
					painelJupiter, "Jupiter");
					// Deixa o painelTerra pr�-selecionado
					tabbedPane.setSelectedIndex(0);
					getContentPane().add(tabbedPane);
					this.setSize(300, 200);
					this.setVisible(true);
					} // fim do construtor
	}

}
