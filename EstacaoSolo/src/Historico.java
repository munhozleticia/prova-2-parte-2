
public class Historico extends Dados {


	private String data;
	private String hora;
	
	public Historico() {
		
	
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

}
